var gulp = require('gulp')
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat')
var sass = require('gulp-sass');
// var sourcemaps = require('gulp-sourcemaps')
// var uglify = require('gulp-uglify')
// var ngAnnotate = require('gulp-ng-annotate')

gulp.task('build', function () {
  gulp.src(['src/js/components/**/*.js', 'src/js/main.js'])
    .pipe(concat('www/js/main.js'))
    //.pipe(ngAnnotate())
    //.pipe(uglify())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('.'))
});

gulp.task('browser-sync', function() {
  browserSync.init({
    proxy: "localhost",
    open: false
  });
});

gulp.task('sass', function () {
  gulp.src('src/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('www/css'));
});

gulp.task('watch', ['build', 'browser-sync'], function () {
  gulp.watch('src/js/**/*.js', ['build'])
  gulp.watch('src/sass/*.scss', ['sass'])
});
