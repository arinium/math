#!/bin/sh
cd /var/public_html
echo "Please wait for npm install to complete..."
npm install
echo "  npm install done."
echo "Starting Nginx..."
nginx
echo "Starting gulp tasks..."
gulp watch
