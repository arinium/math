var App = React.createClass({
  min: 0,
  max: 10,
  optionsArr: new Array(),
  initialState: {
    selectedMultiplier: null,
    multipliers: {},
    speechContent: "Hei, valitse kertotaulu mitä haluat harjoitella."
  },

  getInitialState: function() {
    for (var i = this.min; i <= this.max; i++) {
      this.optionsArr.push(<MultiplierOption multiplier={i} key={i} setSelectedMultiplier={this.setSelectedMultiplier} setSpeechContent={this.setSpeechContent} />);
    }
    return this.initialState;
  },

  resetToInitialState: function() {
    this.setState(this.initialState);
  },

  setSpeechContent: function(speechContent) {
    this.setState({
      speechContent: speechContent
    });
  },

  setSelectedMultiplier: function(multiplier) {
    //console.log('setSelectedMultiplier');
    this.setState({
      selectedMultiplier: multiplier
    }, function() {
      //console.log('setSelectedMultiplier: this.state.selectedMultiplier:', this.state.selectedMultiplier);
      this.getNewNumbers();
      this.forceUpdate();
    });
  },

  getNewNumbers: function(newMultipliers) {
    // console.log('getNewNumbers');
    // Keep generating new random numbers until we have new set of numbers.
    do {
       var newMultipliers = this.generateRandomNumbers();
      //  console.log('this.state.multipliers', this.state.multipliers);
      //  console.log('newMultipliers', newMultipliers.multipliers);
    } while (JSON.stringify(this.state.multipliers) == JSON.stringify(newMultipliers.multipliers));
    this.setState(newMultipliers);
  },

  generateRandomNumbers: function() {
    // console.log('generateRandomNumbers: this.state.selectedMultiplier:', this.state.selectedMultiplier);
    if (this.state.selectedMultiplier == "all") {
      var multiplierOne = Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
    } else {
      var multiplierOne = this.state.selectedMultiplier;
    }
    var multiplierTwo = Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
    var randomizer = Math.random();
    if (randomizer < 0.5) {
      var newMultipliers = {
        'multipliers': {
          multiplierOne: multiplierOne,
          multiplierTwo: multiplierTwo,
          answer: (multiplierOne * multiplierTwo)
        }
      };
    } else {
      var newMultipliers = {
        'multipliers': {
          multiplierOne: multiplierOne,
          multiplierTwo: multiplierTwo,
          answer: (multiplierOne * multiplierTwo)
        }
      };
    }
    return newMultipliers;
  },

  render: function() {
    if (this.state.selectedMultiplier === null) {
      return (
        <div>
          <ul>
            {this.optionsArr}
            <MultiplierOption multiplier="all" key="all" setSelectedMultiplier={this.setSelectedMultiplier} setSpeechContent={this.setSpeechContent} />
          </ul>
          <Mascot speechContent={this.state.speechContent} />
        </div>
      )
    } else {
      return (
        <div>
          <Question
            multiplierOne={this.state.multipliers.multiplierOne}
            multiplierTwo={this.state.multipliers.multiplierTwo}
            answer={this.state.multipliers.answer}
            getNewNumbers={this.getNewNumbers}
            setSpeechContent={this.setSpeechContent}
            resetToInitialState={this.resetToInitialState}
          />
          <Mascot speechContent={this.state.speechContent} />
        </div>
      )
    }
  }
});

var BackButton = React.createClass({
  toStartScreen: function() {
    this.props.resetToInitialState();
  },
  render: function() {
    return(
      <button className="btn btn-default" onClick={this.toStartScreen} type="submit" id="back-button">Takaisin alkuun</button>
    )
  }
});

var AnswerHelp = React.createClass({
  clickHelp: function() {
    this.props.showCorrectAnswer();
  },

  render: function() {
    return(
      <a href="#" id="answer-help" title="Näytä vastaus" onClick={this.clickHelp}><span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>
    )
  }
})

var Mascot = React.createClass({

  render: function() {
    if (this.props.speechContent) {
      var speechBubbelClass = "true";
    } else {
      var speechBubbelClass = "false";
    }
    return(
      <div id="mascot">
        <div id="speech-bubble" className={speechBubbelClass}>{this.props.speechContent}<div id="bubble-pointer"></div></div>
        <div id="mascot-img">
          <img src="img/black_cat.png" />
        </div>
      </div>
    )
  }
});

var MultiplierOption = React.createClass({
  clickItem: function(item, event) {
    this.props.setSelectedMultiplier(this.props.multiplier);
    this.props.setSpeechContent(false);
  },
  render: function() {
    if (this.props.multiplier == "all") {
      return(
        <li className="custom-multiplier-option" key={this.props.multiplier} onClick={this.clickItem}>Kaikki kertotaulut</li>
      )
    } else {
      return(
        <li key={this.props.multiplier} onClick={this.clickItem}>{this.props.multiplier}</li>
      )
    }
  }
});

var Question = React.createClass({
  typeDelay: 2000,
  showRewardDelay: 4000,
  speechContentArr: [
    "Hienoa!",
    "Mahtavaa!",
    "Olet taitava :)",
    "Upeeta!",
    "Hyvä!",
    "Oikein meni!",
    "Tiesit oikein",
    "Loistavaa!",
    "Helmee!",
    "Siistiä, oikein meni!"
  ],
  timeout: null,

  getInitialState: function() {
    return {
      typedAnswer: '',
      correct: false,
      soundSrc: ''
    }
  },

  getSpeechContent: function() {
    var min = 0;
    var max = this.speechContentArr.length - 1;
    var index = Math.floor(Math.random() * (max - min + 1)) + min;
    var speechContent = this.speechContentArr[index];
    return this.speechContentArr[index];
  },

  setSoundSrc: function() {
    var min = 1;
    var max = 4;
    var index = Math.floor(Math.random() * (max - min + 1)) + min;
    var src = 'audio/';
    if (this.state.correct) {
      src += 'correct/';
    } else {
      src += 'wrong/';
    }
    src += index + '.mp3';
    this.setState({soundSrc: src});
  },

  showCorrectAnswer: function() {
    var that = this;
    this.setState({
      typedAnswer: this.props.answer
    }, function() {
        setTimeout(function() {
          that.props.getNewNumbers();
          that.setState({typedAnswer: '', correct: false});
        }, that.showRewardDelay)
      }
    );
  },

  componentDidMount: function() {
    ReactDOM.findDOMNode(this.refs.answerInput).focus();
  },

  checkTheAnswer: function(event) {
    this.props.setSpeechContent(false);
    if (this.timeout) {
      //console.log('clearTimeout: ', this.timeout);
      clearTimeout(this.timeout);
    }
    var typedValue = event.target.value;
    this.setState({typedAnswer: typedValue});
    if (typedValue == '') {
      clearTimeout(this.timeout);
    } else {
      var that = this;
      this.timeout = setTimeout(function() {
        if (typedValue == that.props.answer) {
          clearTimeout(that.timeout);
          that.setState({correct: true});
          // console.log('Correct!');
          that.setSoundSrc();
          that.refs.sound.playSound();
          that.props.setSpeechContent(that.getSpeechContent());
          setTimeout(function() {
            that.setState({typedAnswer: '', correct: false});
            that.props.setSpeechContent(false);
            that.props.getNewNumbers();
          }, that.showRewardDelay);
        } else {
          // console.log('Please try again');
          that.setState({typedAnswer: '', correct: false});
          that.setSoundSrc();
          that.refs.sound.playSound();
          that.props.setSpeechContent('Yritä vielä uudestaan.');
          clearTimeout(that.timeout);
        }
      }, that.typeDelay);
      //console.log('this.timeout: ', this.timeout);
    }
  },

  render: function() {
    //console.log('Question: render()');
    return(
      <div className="question">
        {this.props.multiplierOne} &sdot; {this.props.multiplierTwo} =
        <input className="answer" ref="answerInput" type="number" value={this.state.typedAnswer} onChange={this.checkTheAnswer} placeholder="?" />
        <Reward correct={this.state.correct} />
        <AnswerHelp showCorrectAnswer={this.showCorrectAnswer} />
        <BackButton resetToInitialState={this.props.resetToInitialState} />
        <Sound ref="sound" src={this.state.soundSrc} />
      </div>
    )
  }
});

var Reward = React.createClass({

  render: function() {
    return(
      <div id="check" className={this.props.correct}><span className="glyphicon glyphicon-ok" aria-hidden="true"></span></div>
    );
  }
})

var Sound = React.createClass({

  getInitialState: function() {
    // Fix up prefixing
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    var context = new AudioContext();
    return {context: context};
  },

  playSound: function() {
    var context = this.state.context;
    // console.log(this.state.context);
    var source = context.createBufferSource();
    var request = new XMLHttpRequest();
    request.open('GET', this.props.src, true);
    request.responseType = 'arraybuffer';

    var that = this;
    // Decode asynchronously
    request.onload = function() {
      context.decodeAudioData(request.response, function(buffer) {
        source.buffer = buffer;
        source.connect(context.destination);
        source.start(0);
      }, function(e){"Error with decoding audio data" + e.err});
    }
    request.send();
  },

  render: function() {
    return(
      <div></div>
    )
  }
})

ReactDOM.render(<App />, document.querySelector("#main"));
