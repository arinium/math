var MultiplierOption = React.createClass({
  clickItem: function(item, event) {
    this.props.setSelectedMultiplier(this.props.multiplier);
    this.props.setSpeechContent(false);
  },
  render: function() {
    if (this.props.multiplier == "all") {
      return(
        <li className="custom-multiplier-option" key={this.props.multiplier} onClick={this.clickItem}>Kaikki kertotaulut</li>
      )
    } else {
      return(
        <li key={this.props.multiplier} onClick={this.clickItem}>{this.props.multiplier}</li>
      )
    }
  }
});
