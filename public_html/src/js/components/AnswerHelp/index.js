var AnswerHelp = React.createClass({
  clickHelp: function() {
    this.props.showCorrectAnswer();
  },

  render: function() {
    return(
      <a href="#" id="answer-help" title="Näytä vastaus" onClick={this.clickHelp}><span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>
    )
  }
})
