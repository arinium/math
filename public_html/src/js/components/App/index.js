var App = React.createClass({
  min: 0,
  max: 10,
  optionsArr: new Array(),
  initialState: {
    selectedMultiplier: null,
    multipliers: {},
    speechContent: "Hei, valitse kertotaulu mitä haluat harjoitella."
  },

  getInitialState: function() {
    for (var i = this.min; i <= this.max; i++) {
      this.optionsArr.push(<MultiplierOption multiplier={i} key={i} setSelectedMultiplier={this.setSelectedMultiplier} setSpeechContent={this.setSpeechContent} />);
    }
    return this.initialState;
  },

  resetToInitialState: function() {
    this.setState(this.initialState);
  },

  setSpeechContent: function(speechContent) {
    this.setState({
      speechContent: speechContent
    });
  },

  setSelectedMultiplier: function(multiplier) {
    //console.log('setSelectedMultiplier');
    this.setState({
      selectedMultiplier: multiplier
    }, function() {
      //console.log('setSelectedMultiplier: this.state.selectedMultiplier:', this.state.selectedMultiplier);
      this.getNewNumbers();
      this.forceUpdate();
    });
  },

  getNewNumbers: function(newMultipliers) {
    // console.log('getNewNumbers');
    // Keep generating new random numbers until we have new set of numbers.
    do {
       var newMultipliers = this.generateRandomNumbers();
      //  console.log('this.state.multipliers', this.state.multipliers);
      //  console.log('newMultipliers', newMultipliers.multipliers);
    } while (JSON.stringify(this.state.multipliers) == JSON.stringify(newMultipliers.multipliers));
    this.setState(newMultipliers);
  },

  generateRandomNumbers: function() {
    // console.log('generateRandomNumbers: this.state.selectedMultiplier:', this.state.selectedMultiplier);
    if (this.state.selectedMultiplier == "all") {
      var multiplierOne = Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
    } else {
      var multiplierOne = this.state.selectedMultiplier;
    }
    var multiplierTwo = Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
    var randomizer = Math.random();
    if (randomizer < 0.5) {
      var newMultipliers = {
        'multipliers': {
          multiplierOne: multiplierOne,
          multiplierTwo: multiplierTwo,
          answer: (multiplierOne * multiplierTwo)
        }
      };
    } else {
      var newMultipliers = {
        'multipliers': {
          multiplierOne: multiplierOne,
          multiplierTwo: multiplierTwo,
          answer: (multiplierOne * multiplierTwo)
        }
      };
    }
    return newMultipliers;
  },

  render: function() {
    if (this.state.selectedMultiplier === null) {
      return (
        <div>
          <ul>
            {this.optionsArr}
            <MultiplierOption multiplier="all" key="all" setSelectedMultiplier={this.setSelectedMultiplier} setSpeechContent={this.setSpeechContent} />
          </ul>
          <Mascot speechContent={this.state.speechContent} />
        </div>
      )
    } else {
      return (
        <div>
          <Question
            multiplierOne={this.state.multipliers.multiplierOne}
            multiplierTwo={this.state.multipliers.multiplierTwo}
            answer={this.state.multipliers.answer}
            getNewNumbers={this.getNewNumbers}
            setSpeechContent={this.setSpeechContent}
            resetToInitialState={this.resetToInitialState}
          />
          <Mascot speechContent={this.state.speechContent} />
        </div>
      )
    }
  }
});
