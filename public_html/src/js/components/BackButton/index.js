var BackButton = React.createClass({
  toStartScreen: function() {
    this.props.resetToInitialState();
  },
  render: function() {
    return(
      <button className="btn btn-default" onClick={this.toStartScreen} type="submit" id="back-button">Takaisin alkuun</button>
    )
  }
});
