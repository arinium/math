var Mascot = React.createClass({

  render: function() {
    if (this.props.speechContent) {
      var speechBubbelClass = "true";
    } else {
      var speechBubbelClass = "false";
    }
    return(
      <div id="mascot">
        <div id="speech-bubble" className={speechBubbelClass}>{this.props.speechContent}<div id="bubble-pointer"></div></div>
        <div id="mascot-img">
          <img src="img/black_cat.png" />
        </div>
      </div>
    )
  }
});
