var Sound = React.createClass({

  getInitialState: function() {
    // Fix up prefixing
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    var context = new AudioContext();
    return {context: context};
  },

  playSound: function() {
    var context = this.state.context;
    // console.log(this.state.context);
    var source = context.createBufferSource();
    var request = new XMLHttpRequest();
    request.open('GET', this.props.src, true);
    request.responseType = 'arraybuffer';

    var that = this;
    // Decode asynchronously
    request.onload = function() {
      context.decodeAudioData(request.response, function(buffer) {
        source.buffer = buffer;
        source.connect(context.destination);
        source.start(0);
      }, function(e){"Error with decoding audio data" + e.err});
    }
    request.send();
  },

  render: function() {
    return(
      <div></div>
    )
  }
})
