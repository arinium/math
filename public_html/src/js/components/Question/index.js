var Question = React.createClass({
  typeDelay: 2000,
  showRewardDelay: 4000,
  speechContentArr: [
    "Hienoa!",
    "Mahtavaa!",
    "Olet taitava :)",
    "Upeeta!",
    "Hyvä!",
    "Oikein meni!",
    "Tiesit oikein",
    "Loistavaa!",
    "Helmee!",
    "Siistiä, oikein meni!"
  ],
  timeout: null,

  getInitialState: function() {
    return {
      typedAnswer: '',
      correct: false,
      soundSrc: ''
    }
  },

  getSpeechContent: function() {
    var min = 0;
    var max = this.speechContentArr.length - 1;
    var index = Math.floor(Math.random() * (max - min + 1)) + min;
    var speechContent = this.speechContentArr[index];
    return this.speechContentArr[index];
  },

  setSoundSrc: function() {
    var min = 1;
    var max = 4;
    var index = Math.floor(Math.random() * (max - min + 1)) + min;
    var src = 'audio/';
    if (this.state.correct) {
      src += 'correct/';
    } else {
      src += 'wrong/';
    }
    src += index + '.mp3';
    this.setState({soundSrc: src});
  },

  showCorrectAnswer: function() {
    var that = this;
    this.setState({
      typedAnswer: this.props.answer
    }, function() {
        setTimeout(function() {
          that.props.getNewNumbers();
          that.setState({typedAnswer: '', correct: false});
        }, that.showRewardDelay)
      }
    );
  },

  componentDidMount: function() {
    ReactDOM.findDOMNode(this.refs.answerInput).focus();
  },

  checkTheAnswer: function(event) {
    this.props.setSpeechContent(false);
    if (this.timeout) {
      //console.log('clearTimeout: ', this.timeout);
      clearTimeout(this.timeout);
    }
    var typedValue = event.target.value;
    this.setState({typedAnswer: typedValue});
    if (typedValue == '') {
      clearTimeout(this.timeout);
    } else {
      var that = this;
      this.timeout = setTimeout(function() {
        if (typedValue == that.props.answer) {
          clearTimeout(that.timeout);
          that.setState({correct: true});
          // console.log('Correct!');
          that.setSoundSrc();
          that.refs.sound.playSound();
          that.props.setSpeechContent(that.getSpeechContent());
          setTimeout(function() {
            that.setState({typedAnswer: '', correct: false});
            that.props.setSpeechContent(false);
            that.props.getNewNumbers();
          }, that.showRewardDelay);
        } else {
          // console.log('Please try again');
          that.setState({typedAnswer: '', correct: false});
          that.setSoundSrc();
          that.refs.sound.playSound();
          that.props.setSpeechContent('Yritä vielä uudestaan.');
          clearTimeout(that.timeout);
        }
      }, that.typeDelay);
      //console.log('this.timeout: ', this.timeout);
    }
  },

  render: function() {
    //console.log('Question: render()');
    return(
      <div className="question">
        {this.props.multiplierOne} &sdot; {this.props.multiplierTwo} =
        <input className="answer" ref="answerInput" type="number" value={this.state.typedAnswer} onChange={this.checkTheAnswer} placeholder="?" />
        <Reward correct={this.state.correct} />
        <AnswerHelp showCorrectAnswer={this.showCorrectAnswer} />
        <BackButton resetToInitialState={this.props.resetToInitialState} />
        <Sound ref="sound" src={this.state.soundSrc} />
      </div>
    )
  }
});
