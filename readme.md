## Pre-requisites ##

* npm
* docker
* docker-compose

## Installation ##

```
#!sh
$ npm install
```

## Running development instance ##

```
#!sh

$ ./start_dev.sh
```

## Stopping development instance ##

```
#!sh
$ ^C [Ctrl+C]
$ ./stop_dev.sh
```
