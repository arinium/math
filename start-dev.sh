#!/bin/bash

echo "Creating Docker containers..."
docker-compose create
echo "  done."
echo "Starting Docker containers..."
docker-compose start
echo "  done."

echo "Running \"npm run build\""
npm run build
